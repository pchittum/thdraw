import {Command, flags} from '@oclif/command'

const fs = require('fs')
const path = require('path')

class Thdraw extends Command {
  static description = 'Output a ASCII art Trailhead Characters.'

  static flags = {
    // add --version flag to show CLI version
    version: flags.version({char: 'v'}),
    help: flags.help({char: 'h'}),
    character: flags.string({
      char: 'c',
      description: 'Which character to draw.',
      required: true,
      options: ['hootie', 'metamoose', 'flogo', 'flogo2', 'astro', 'codey', 'saasy', 'cloudy', 'appy'],
      parse: input => input.toLowerCase(),
      default: 'flogo'
    }),
    story: flags.boolean({char: 'y', description: 'Also output the character story.'}),
    storyonly: flags.boolean({char: 'o', description: 'Skip the character and just output the character story.'})
  }

  static args = [{name: 'file'}]

  async run() {
    //no args being used, so skip assignment
    const {flags} = this.parse(Thdraw)

    if (flags.character) {
      drawFile(flags.character)
    }
  }
}

export = Thdraw

function drawFile(characterName: string): void {
  const filePath = path.join('.', 'data', characterName)

  fs.readFile(filePath, function (err: any, data: any) {
    if (err) {
      throw err
    } else if (data) {
      // tslint:disable-next-line:no-console
      console.log(data.toString())
    }

  })
}

// maybe externalise this as additional character data
// tslint:disable-next-line:no-unused
const characters = {
  hootie : {
    name: 'Hootie',
    story: 'Hootie is an owl, not a blowfish.',
    catchphrase: 'Hoo hoo!',
    pronouns: 'He/Him',
    artPath: ['data', 'hootie']},
  flogo: {
    name: 'Flogo',
    story: 'Once upon a time a game-happy evangelist hacked together an idea...',
    catchphrase: 'none',
    pronouns: 'it',
    artPath: ['data', 'flogo']
  },
  metamoose : {
    name: 'Meta Moose',
    story: 'I am a moose about mooses.',
    catchphrase: 'Shoot for the moon!',
    pronouns: 'They/Them',
    artPath: ['data', 'metamoose']
  },
  astro: {
    artPath: ['data', 'astro']
  }
}
