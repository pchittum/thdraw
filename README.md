thdraw
======

Draw ASCII art of characters from Trailhead

This project is a POC that was to accomplish two things:

* Create a first project that would build something "meaningful" with oclif beyond just another hello world.
* Possibly serve as a building block to create learning content related to oclif. 

[![oclif](https://img.shields.io/badge/cli-oclif-brightgreen.svg)](https://oclif.io)
[![Version](https://img.shields.io/npm/v/thdraw.svg)](https://npmjs.org/package/thdraw)
[![Downloads/week](https://img.shields.io/npm/dw/thdraw.svg)](https://npmjs.org/package/thdraw)
[![License](https://img.shields.io/npm/l/thdraw.svg)](https://github.com/pchittum/thdraw/blob/master/package.json)

<!-- toc -->
# Usage
<!-- usage -->
# Commands
<!-- commands -->
